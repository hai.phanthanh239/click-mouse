using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    [SerializeField]
    private float speed;
    public ParticleSystem heartEffect;
    ParticleSystem parsys;

    float xBegin, xEnd, fixSpeed;
    bool isMoveRight = true;

    private void Awake()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        parsys = Instantiate(heartEffect, transform.position, Quaternion.identity);
        parsys.loop = true;
        fixSpeed = speed;
        xBegin = transform.position.x - 1;
        xEnd = transform.position.x + 1;
        
    }

    // Update is called once per frame
    private void Update()
    {
        if (transform.position.x < xBegin) isMoveRight = true;
        if (transform.position.x > xEnd) isMoveRight = false;

        if (transform.position.x < xBegin + 0.2f || transform.position.x > xEnd - 0.2f) fixSpeed = speed / 2;
        else fixSpeed = speed;
        parsys.transform.position = transform.position;
    }
    
    void FixedUpdate()
    {
        if (isMoveRight)
        {
            transform.Translate(new Vector3(fixSpeed * Time.fixedDeltaTime, 0, 0));
        }
        else
        {
            transform.Translate(new Vector3(-fixSpeed * Time.fixedDeltaTime, 0, 0));
        }
    }

    private void OnMouseDown()
    {
        GameObject gameController;
        gameController = GameObject.FindGameObjectWithTag("GameController");
        gameController.GetComponent<GameController>().getItemHeart();
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        parsys.loop = false;
    }
}
