using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1 : MonoBehaviour
{
    Animator anim;
    public GameObject fruit1, fruit2, fruit3, fruit4, pop, plus10;
    GameObject fruit;

    public AudioClip shotClip;
    AudioSource music;

    public int HP;
    float targetAngles; //goc quay ma boss se ban dan

    bool isStopFight;

    public bool isTimeMode;

    private void Awake()
    {
        anim = gameObject.GetComponent<Animator>();
        music = gameObject.GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        isStopFight = false;
        targetAngles = transform.eulerAngles.z;
        Invoke("RandomTargetAngles", 1.5f); //moi vao xuat hien trong 1.5s, sau do hanh dong
        Invoke("RunAway", 10);
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space)) Shot();
    }
    void FixedUpdate()
    {
        transform.eulerAngles = new Vector3(0, 0, Mathf.Lerp(transform.eulerAngles.z, targetAngles, Time.fixedDeltaTime*10));      
    }

    void RandomTargetAngles()
    {
        if (!isStopFight)
        {
            targetAngles = Random.Range(0, 360);
            Invoke("RandomTargetAngles", 1);
            Invoke("Shot", 0.3f);
        }
    }

    void Shot()
    {
        if (!isStopFight)
        {
            anim.Play("Attack");
            int rand = Random.Range(0, 5);
            if (rand <= 1) fruit = fruit1;
            else if (rand <= 2) fruit = fruit2;
            else if (rand <= 3) fruit = fruit3;
            else if (rand <= 5) fruit = fruit4;

            fruit.GetComponent<Fruit>().isBossShot = true;

            Instantiate(fruit, transform.position, transform.rotation);
            fruit.GetComponent<Fruit>().isBossShot = false;
            Invoke("ShotSound", 0.15f);
        }
    }
    void ShotSound()
    {
        music.clip = shotClip;
        music.Play();
    }

    private void OnMouseDown()
    {
        if (!isStopFight)
        {
            music.clip = shotClip;
            music.Play();
            anim.Play("get_hit");
            HP--;
            if (HP <= 0)
            {
                Instantiate(pop, transform.position, Quaternion.identity);
                Instantiate(plus10, transform.position, Quaternion.identity);
                if (isTimeMode) GameCtrlTimeMode.point += 10;
                else GameController.point += 10;
                Destroy(gameObject);
            }
        }
    }

    void RunAway()
    {
        targetAngles = 0;
        isStopFight = true;
        anim.Play("run_away");
        Destroy(gameObject, 2);
    }

    private void OnDestroy()
    {
        
    }

}
