using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnscaledTimeParticle : MonoBehaviour
{
    private ParticleSystem ps;
    //public float hSliderValue = 1.0f;
    //public bool useUnscaledTime = false;

    void Start()
    {
        ps = GetComponent<ParticleSystem>();

        var main = ps.main;
        main.useUnscaledTime = true;
    }

    void Update()
    {
        //var main = ps.main;
        ////main.useUnscaledTime = useUnscaledTime;
        //main.useUnscaledTime = true;
        ////Time.timeScale = hSliderValue;
    }
}
