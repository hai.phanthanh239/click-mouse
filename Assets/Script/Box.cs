using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Box : MonoBehaviour
{
    [SerializeField]
    private string fruitTag;
    [SerializeField]
    private GameObject plusPoint, minusPoint;
    [SerializeField]
    private Sprite openBoxSprite;
    Sprite defaultSprite;
    SpriteRenderer sprRen;

    public AudioClip obtainClip, refuseClip;
    AudioSource music;

    public bool isTimeMode;

    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        sprRen = gameObject.GetComponent<SpriteRenderer>();
        anim = gameObject.GetComponent<Animator>();
        music = gameObject.GetComponent<AudioSource>();
        defaultSprite = sprRen.sprite;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Item")
        {
            if (other.tag == fruitTag)
            {
                music.clip = obtainClip;
                anim.Play("obtain");
                //tang diem
                if (!isTimeMode)
                    GameController.point++;
                else GameCtrlTimeMode.point++;

                Instantiate(plusPoint, other.transform.position, Quaternion.identity);
            }
            else
            {
                music.clip = refuseClip;
                anim.Play("not_obtain");
                //tru diem
                if (!isTimeMode)
                    GameController.point--;
                else GameCtrlTimeMode.point++;

                Instantiate(minusPoint, other.transform.position, Quaternion.identity);
            }
            music.Play();
            Destroy(other.gameObject);
        }
    }
    private void OnMouseEnter()
    {
        sprRen.sprite = openBoxSprite;
    }
    private void OnMouseExit()
    {
        sprRen.sprite = defaultSprite;
    }
}
