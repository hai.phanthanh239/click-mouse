using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour
{
    public static float point = 0, HP;
    float maxHP = 5;
    public static bool isClickStar = false; //kiem tra xem co nhan vao item Star khong
    public static string tagCursorGet = null; //tag con tro nhan duoc khi con tro nhan vao mot fruit nao do
    public Text pointText, pointGameOverText;
    public Slider slider;

    public GameObject gameOverPanel, blackScene;
    Animator blackSceneAnim;
    public Button restartBtn, backBtn;
    public Sprite restartBtnHover, backBtnHover;
    Sprite defaultRestartBtnSprite, defaultBackBtnSprite;
    Animator restartBtnAnim, backBtnAnim;

    public AudioClip getItemClip, touchClip;
    AudioSource music;
    public ParticleSystem clickEffect;

    bool isStart = false, isGameover = false;

    private void Awake()
    {
        blackSceneAnim = blackScene.GetComponent<Animator>();
        music = gameObject.GetComponent<AudioSource>();
        restartBtnAnim = restartBtn.GetComponent<Animator>();
        backBtnAnim = backBtn.GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {
        defaultRestartBtnSprite = restartBtn.image.sprite;
        defaultBackBtnSprite = backBtn.image.sprite;
        blackSceneAnim.Play("BlackSceneBegin");
        gameOverPanel.SetActive(false);
        Time.timeScale = 0;
        HP = maxHP;
        slider.maxValue = maxHP;
        point = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) //Hieu ung khi click chuot
        {
            music.clip = touchClip;
            music.Play();
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z += 10;
            Instantiate(clickEffect, mousePos, Quaternion.identity);
        }

        if (Input.GetMouseButton(0) && isStart == false)
        {
            Time.timeScale = 1;
            isStart = true;
        }
        if (point < 0) point = 0;
        if (HP <= 0 && isGameover == false) GameOver();
        if (HP > maxHP) HP = maxHP;

        slider.value = HP;
        pointText.text = "Point: " + point.ToString();
        if (isClickStar) Invoke("ResetClickStar", 0.2f);
      
        if (Input.GetMouseButtonUp(0)) tagCursorGet = null;
    }

    private void ResetClickStar()
    {
        isClickStar = false;
    }

    public void BackBtn()
    {
        Time.timeScale = 1;
        blackSceneAnim.Play("BlackSceneEnd");
        Invoke("LoadMenuScene", 1);
        //SceneManager.LoadScene(0);
    }
    public void ReStartBtn()
    {
        Time.timeScale = 1;
        blackSceneAnim.Play("BlackSceneEnd");
        Invoke("LoadThisScene", 1);
        //SceneManager.LoadScene(1);
    }

    void LoadThisScene()
    {
        Debug.Log("LOAD");
        SceneManager.LoadScene(1);
    }

    void LoadMenuScene()
    {
        SceneManager.LoadScene(0);
    }

    public void onHoverRestartBtn()
    {
        restartBtnAnim.Play("BtnEnter");
        music.clip = touchClip;
        music.Play();
        restartBtn.image.sprite = restartBtnHover;
    }

    public void onExitRestartBtn()
    {
        restartBtnAnim.Play("BtnExit");
        restartBtn.image.sprite = defaultRestartBtnSprite;
    }

    public void onHoverBackBtn()
    {
        backBtnAnim.Play("BtnEnter");
        music.clip = touchClip;
        music.Play();
        backBtn.image.sprite = backBtnHover;
    }
    public void onExitBackBtn()
    {
        backBtnAnim.Play("BtnExit");
        backBtn.image.sprite = defaultBackBtnSprite;
    }

    public void getItemHeart()
    {
        music.clip = getItemClip;
        music.Play();
        HP++;
    }
    public void getItemStar()
    {
        music.clip = getItemClip;
        music.Play();
        isClickStar = true;
    }

    void GameOver()
    {
        HP = 0;
        isGameover = true;
        Debug.Log("GAME OVER");
        Invoke("Stop", 1);
    }

    void Stop()
    {
        pointGameOverText.text = "YOUR POINT: " + point.ToString();
        GameObject cameraObj = GameObject.FindGameObjectWithTag("MainCamera");
        cameraObj.GetComponent<AudioSource>().Stop();
        gameOverPanel.SetActive(true);
        Time.timeScale = 0;
    }
}
