using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObj : MonoBehaviour
{
    GameObject cameraObj;
    public AudioClip DieClip;
    AudioSource music;

    public bool isTimeMode;
    // Start is called before the first frame update
    void Start()
    {
        music = gameObject.GetComponent<AudioSource>();
        cameraObj = GameObject.FindGameObjectWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Boss") Destroy(other.gameObject);
        if (other.tag != "Item" && other.tag != "Boss")
        {
            music.clip = DieClip;
            music.Play();
            cameraObj.GetComponent<CameraController>().Shake();
            if (!isTimeMode)
            {
                GameController.HP--;
                Debug.Log("HP REMAINING: " + GameController.HP);
            }
            else GameCtrlTimeMode.point--;
        }
    }
}
