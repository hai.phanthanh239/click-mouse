using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Animator anim;

    private void Awake()
    {
        anim = gameObject.GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {
        //transform.position = new Vector3(0, -20, -10);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Shake()
    {
        transform.position = new Vector3(0, 0, -10);
        anim.Play("Shake");
    }

    public void Zoom()
    {
        anim.Play("Zoom");
    }
}
