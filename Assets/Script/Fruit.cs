using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruit : MonoBehaviour
{
    GameObject obj;
    Rigidbody2D rigi;
    Animator anim;

    Vector3 mousePos;
    [SerializeField]
    private float speed;

    public ParticleSystem mergeEffect;

    public bool isInBubble; // kiem tra xem trai cay nay co phai la trai cay nam trong bong bong ko
    public bool isBossShot; // kiem tra xem trai cay nay co phai la trai cay duoc ban tu boss khong
    public bool isBlack; // neu no la gioi nuoc mau den
    [SerializeField]
    private GameObject plusPoint, shader;
    GameObject shaderObj;
    [SerializeField]
    private Sprite holdSprite, hoverBlackSprite; //tra ve sprite khi hold va sprite khi enter (neu no la giot nuoc mau den)
    Sprite defaultSprite;
    SpriteRenderer sprRen;

    public AudioClip touchClip;
    AudioSource music;

    bool isMerge, isCanClick = true;
    public bool isTimeMode;

    private void Awake()
    {
        obj = gameObject;
        rigi = obj.GetComponent<Rigidbody2D>();
        anim = obj.GetComponent<Animator>();
        sprRen = obj.GetComponent<SpriteRenderer>();
        music = gameObject.GetComponent<AudioSource>();

        shaderObj = Instantiate(shader, transform.position, Quaternion.identity);

        if (isBossShot)
        {
            isCanClick = false;
            Invoke("CanClick", 0.3f);
            anim.Play("New State");
            shaderObj.GetComponent<Animator>().Play("New State");
            rigi.gravityScale = 0;
            rigi.drag = 0;
            rigi.velocity = new Vector3(speed * Mathf.Cos((transform.rotation.eulerAngles.z-90) * Mathf.PI / 180), speed * Mathf.Sin((transform.rotation.eulerAngles.z-90) * Mathf.PI / 180), 0);
        }
        else if (isInBubble)
        {
            anim.Play("New State");
            shaderObj.GetComponent<Animator>().Play("New State");
            Fall();
        }
        else
        {
            rigi.gravityScale = 0;
            Invoke("Fall", 1f);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        defaultSprite = sprRen.sprite;
        isMerge = false;
        music.clip = touchClip;
    }

    // Update is called once per frame
    void Update()
    {
        if (isTimeMode)
        {
            if (GameCtrlTimeMode.tagCursorGet != obj.tag && isMerge == true)
            {
                isMerge = false; //ko dc de trong OnMouseUp() vi OnMouseUp chi co tac dung len 1 doi tuong
                rigi.gravityScale = 0.3f;
                rigi.velocity = Vector2.zero;
                sprRen.sprite = defaultSprite;
                anim.Play("New State");
            }
            if (isMerge) ToMousePos(1);
            shaderObj.transform.position = transform.position;

            if (GameCtrlTimeMode.isClickStar)
            {
                GameCtrlTimeMode.point++;
                Instantiate(plusPoint, transform.position, Quaternion.identity);
                Destroy(obj);
            }
        }
        else
        {
            if (GameController.tagCursorGet != obj.tag && isMerge == true)
            {
                isMerge = false; //ko dc de trong OnMouseUp() vi OnMouseUp chi co tac dung len 1 doi tuong
                rigi.gravityScale = 0.3f;
                rigi.velocity = Vector2.zero;
                sprRen.sprite = defaultSprite;
                anim.Play("New State");
            }
            if (isMerge) ToMousePos(1);
            shaderObj.transform.position = transform.position;

            if (GameController.isClickStar)
            {
                GameController.point++;
                Instantiate(plusPoint, transform.position, Quaternion.identity);
                Destroy(obj);
            }
        }
    }

    void CanClick()
    {
        isCanClick = true;
    }
    void Fall()
    {
        rigi.gravityScale = 0.3f;
    }
   
    float deltaX;
    float deltaY;
    private void OnMouseDown()
    {
        if (isCanClick)
        {
            if (isTimeMode) GameCtrlTimeMode.tagCursorGet = obj.tag;
            else GameController.tagCursorGet = obj.tag;
            isMerge = true;
        }
    }

    private void OnMouseUp()
    {
        if (isTimeMode) GameCtrlTimeMode.tagCursorGet = obj.tag;
        else GameController.tagCursorGet = null;
    }

    private void OnMouseEnter()
    {
        if (isBlack) sprRen.sprite = hoverBlackSprite;
        if (isCanClick)
        {
            if (isTimeMode)
            {
                if (GameCtrlTimeMode.tagCursorGet == obj.tag && isMerge == false) //isMerge==false ko quan trong, chi anh huong den hieu ung mergeEffect
                {
                    music.Play();

                    Instantiate(mergeEffect, transform.position, Quaternion.identity);
                    mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    mousePos.z += 10;

                    deltaX = mousePos.x - transform.position.x;
                    deltaY = mousePos.y - transform.position.y;
                    isMerge = true;
                }
            }
            else
            {
                if (GameController.tagCursorGet == obj.tag && isMerge == false) //isMerge==false ko quan trong, chi anh huong den hieu ung mergeEffect
                {
                    music.Play();

                    Instantiate(mergeEffect, transform.position, Quaternion.identity);
                    mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    mousePos.z += 10;

                    deltaX = mousePos.x - transform.position.x;
                    deltaY = mousePos.y - transform.position.y;
                    isMerge = true;
                }
            }
        }
    }

    private void OnMouseDrag()
    {
        if (isCanClick) ToMousePos(0);
    }
    private void OnMouseExit()
    {
        if (isBlack) sprRen.sprite = defaultSprite;
    }
    void ToMousePos(int i)
    {
        anim.Play("Hold");
        sprRen.sprite = holdSprite;
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z += 10;
        if (i != 0)
        {
            mousePos.x -= deltaX;
            mousePos.y -= deltaY;           
        }
        obj.transform.position = mousePos;
    }

    private void OnDestroy()
    {
        Destroy(shaderObj);
    }
}
