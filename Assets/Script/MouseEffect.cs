using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseEffect : MonoBehaviour
{
    Vector3 mousePos;
    public Texture2D defaltCursor;
    public float dx, dy; //tang/giam di 1 khoang so voi hot pot cua cursor
    // Start is called before the first frame update
    void Start()
    {
        Cursor.SetCursor(defaltCursor, Vector2.zero, CursorMode.ForceSoftware);
    }

    // Update is called once per frame
    void Update()
    {
        
        //Screen.lockCursor = false;
        //Cursor.lockState = CursorLockMode.Confined;
        //Cursor.visible = false;

        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z += 10;
        mousePos.x += dx;
        mousePos.y -= dy;
        transform.position = mousePos;
    }
}
