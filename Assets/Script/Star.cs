using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{
    [SerializeField]
    private float speed;
    public ParticleSystem shinyEffect;
    ParticleSystem parsys;
    bool isMoveRight = false;

    public bool isTimeMode;
    // Start is called before the first frame update
    void Start()
    {       
        parsys = Instantiate(shinyEffect, transform.position, Quaternion.identity);
        parsys.loop = true;
        if (Random.Range(0, 10) < 5) isMoveRight = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isMoveRight) transform.Translate(new Vector3(speed / 3 * Time.fixedDeltaTime, -speed * Time.fixedDeltaTime, 0));
        else transform.Translate(new Vector3(-speed/3 * Time.fixedDeltaTime, -speed * Time.fixedDeltaTime, 0));
        parsys.transform.position = transform.position;
    }

    private void OnMouseDown()
    {
        GameObject gameController;
        gameController = GameObject.FindGameObjectWithTag("GameController");
        if (isTimeMode) gameController.GetComponent<GameCtrlTimeMode>().getItemStar();
        else gameController.GetComponent<GameController>().getItemStar();
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        parsys.loop = false;
    }
}
