using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitBubble : MonoBehaviour
{
    //Animator anim;
    GameObject obj;
    Rigidbody2D rigi;

    [SerializeField]
    private float speed;
    float fixSpeed;
    
    public GameObject fruit, pop;
    [SerializeField]
    private GameObject plusPoint;

    float xBegin, xEnd;
    bool isMoveRight = true;

    public bool isTimeMode;

    private void Awake()
    {
        obj = gameObject;
        rigi = obj.GetComponent<Rigidbody2D>();
        
    }
    // Start is called before the first frame update
    void Start()
    {
        fixSpeed = speed;
        xBegin = transform.position.x - 1;
        xEnd = transform.position.x + 1;
        if (Random.Range(0, 10) < 5) isMoveRight = false;
    }

    // Update is called once per frame
    private void Update()
    {
        if (isTimeMode)
        {
            if (GameCtrlTimeMode.isClickStar)
            {
                GameCtrlTimeMode.point++;
                Instantiate(plusPoint, transform.position, Quaternion.identity);
                Destroy(obj);
            }
        }
        else
        {
            if (GameController.isClickStar)
            {
                GameController.point++;
                Instantiate(plusPoint, transform.position, Quaternion.identity);
                Destroy(obj);
            }
        }

        if (transform.position.x < xBegin) isMoveRight = true;
        if (transform.position.x > xEnd) isMoveRight = false;

        if (transform.position.x < xBegin + 0.2f || transform.position.x > xEnd - 0.2f) fixSpeed = speed / 2;
        else fixSpeed = speed;
    }
    void FixedUpdate()
    {
        if (isMoveRight)
        {
            transform.Translate(new Vector3(fixSpeed * Time.fixedDeltaTime, 0, 0));
        }
        else
        {
            transform.Translate(new Vector3(-fixSpeed * Time.fixedDeltaTime, 0, 0));
        }
    }

    void Fall()
    {
        rigi.gravityScale = 0.3f;
    }
    private void OnMouseDown()
    {
        Debug.Log("Bong bong bien thanh fruit");
        fruit.GetComponent<Fruit>().isInBubble = true;
        Instantiate(fruit, transform.position, Quaternion.identity);
        fruit.GetComponent<Fruit>().isInBubble = false;
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        Instantiate(pop, transform.position, Quaternion.identity);
    }
}
