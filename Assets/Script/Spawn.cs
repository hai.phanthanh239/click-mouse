using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject fruit1, fruit2, fruit3, fruit4, redBubble, greenBubble, purpleBubble, yellowBubble, bigRed, bigGreen, bigPurple, bigYellow,
        redBlack, greenBlack, purpleBlack, yellowBlack, heart, star, boss1, boss2, cloud1, cloud2;
    GameObject objSpawn;
    public float timeSpawn;
    float oldTimeSpawn;
    public int count;
    public bool isTimeMode;
    bool isFirst = true;

    void Start()
    {
        Invoke("SpawnObj", 1);
        Invoke("Count", 1);
        oldTimeSpawn = timeSpawn;

        if (isTimeMode)
        {
            fruit1.GetComponent<Fruit>().isTimeMode = true;
            fruit2.GetComponent<Fruit>().isTimeMode = true;
            fruit3.GetComponent<Fruit>().isTimeMode = true;
            fruit4.GetComponent<Fruit>().isTimeMode = true;
            redBlack.GetComponent<Fruit>().isTimeMode = true;
            greenBlack.GetComponent<Fruit>().isTimeMode = true;
            purpleBlack.GetComponent<Fruit>().isTimeMode = true;
            yellowBlack.GetComponent<Fruit>().isTimeMode = true;
            redBubble.GetComponent<FruitBubble>().isTimeMode = true;
            greenBubble.GetComponent<FruitBubble>().isTimeMode = true;
            purpleBubble.GetComponent<FruitBubble>().isTimeMode = true;
            yellowBubble.GetComponent<FruitBubble>().isTimeMode = true;
            bigRed.GetComponent<BigFruit>().isTimeMode = true;
            bigGreen.GetComponent<BigFruit>().isTimeMode = true;
            bigPurple.GetComponent<BigFruit>().isTimeMode = true;
            bigYellow.GetComponent<BigFruit>().isTimeMode = true;
            boss1.GetComponent<Boss1>().isTimeMode = true;
            boss2.GetComponent<Boss2>().isTimeMode = true;
            star.GetComponent<Star>().isTimeMode = true;
        }
        else
        {
            fruit1.GetComponent<Fruit>().isTimeMode = false;
            fruit2.GetComponent<Fruit>().isTimeMode = false;
            fruit3.GetComponent<Fruit>().isTimeMode = false;
            fruit4.GetComponent<Fruit>().isTimeMode = false;
            redBlack.GetComponent<Fruit>().isTimeMode = false;
            greenBlack.GetComponent<Fruit>().isTimeMode = false;
            purpleBlack.GetComponent<Fruit>().isTimeMode = false;
            yellowBlack.GetComponent<Fruit>().isTimeMode = false;
            redBubble.GetComponent<FruitBubble>().isTimeMode = false;
            greenBubble.GetComponent<FruitBubble>().isTimeMode = false;
            purpleBubble.GetComponent<FruitBubble>().isTimeMode = false;
            yellowBubble.GetComponent<FruitBubble>().isTimeMode = false;
            bigRed.GetComponent<BigFruit>().isTimeMode = false;
            bigGreen.GetComponent<BigFruit>().isTimeMode = false;
            bigPurple.GetComponent<BigFruit>().isTimeMode = false;
            bigYellow.GetComponent<BigFruit>().isTimeMode = false;
            boss1.GetComponent<Boss1>().isTimeMode = false;
            boss2.GetComponent<Boss2>().isTimeMode = false;
            star.GetComponent<Star>().isTimeMode = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SpawnObj()
    {
        int rand = Random.Range(0, 100);

        if (!isTimeMode)
        {
            //MAIN MODE
            if (count < 1) objSpawn = null;
            else if (count < 9)
            {
                Debug.Log("wave1");
                if (rand < 25) objSpawn = fruit1;
                else if (rand < 50) objSpawn = fruit2;
                else if (rand < 75) objSpawn = fruit3;
                else if (rand < 100) objSpawn = fruit4;
            }
            else if (count < 17)
            {
                Debug.Log("wave2");
                if (rand < 15) objSpawn = fruit1;
                else if (rand < 30) objSpawn = fruit2;
                else if (rand < 45) objSpawn = fruit3;
                else if (rand < 60) objSpawn = fruit4;
                else if (rand < 70) objSpawn = redBubble;
                else if (rand < 80) objSpawn = greenBubble;
                else if (rand < 90) objSpawn = purpleBubble;
                else if (rand < 100) objSpawn = yellowBubble;
            }
            else if (count < 26)
            {
                Debug.Log("wave3");
                if (rand < 10) objSpawn = fruit1;
                else if (rand < 20) objSpawn = fruit2;
                else if (rand < 30) objSpawn = fruit3;
                else if (rand < 40) objSpawn = fruit4;
                else if (rand < 52) objSpawn = redBubble;
                else if (rand < 65) objSpawn = greenBubble;
                else if (rand < 78) objSpawn = purpleBubble;
                else if (rand < 95) objSpawn = yellowBubble;
                else if (rand < 98) objSpawn = heart;
                else if (rand < 100) objSpawn = star;
            }
            else if (count < 34)
            {
                Debug.Log("wavenull");
                objSpawn = null;
            }
            else if (count < 45)
            {
                Debug.Log("wave4");
                if (rand < 10) objSpawn = fruit1;
                else if (rand < 20) objSpawn = fruit2;
                else if (rand < 30) objSpawn = fruit3;
                else if (rand < 40) objSpawn = fruit4;
                else if (rand < 50) objSpawn = redBubble;
                else if (rand < 60) objSpawn = greenBubble;
                else if (rand < 70) objSpawn = purpleBubble;
                else if (rand < 80) objSpawn = yellowBubble;
                else if (rand < 82) objSpawn = redBlack;
                else if (rand < 84) objSpawn = greenBlack;
                else if (rand < 88) objSpawn = purpleBlack;
                else if (rand < 95) objSpawn = yellowBlack;
                else if (rand < 98) objSpawn = heart;
                else if (rand < 100) objSpawn = star;
            }
            else if (count < 60)
            {
                Debug.Log("wave5");
                if (rand < 10) objSpawn = fruit1;
                else if (rand < 20) objSpawn = fruit2;
                else if (rand < 30) objSpawn = fruit3;
                else if (rand < 40) objSpawn = fruit4;
                else if (rand < 45) objSpawn = redBubble;
                else if (rand < 50) objSpawn = greenBubble;
                else if (rand < 55) objSpawn = purpleBubble;
                else if (rand < 60) objSpawn = yellowBubble;
                else if (rand < 75) objSpawn = redBlack;
                else if (rand < 80) objSpawn = greenBlack;
                else if (rand < 86) objSpawn = purpleBlack;
                else if (rand < 95) objSpawn = yellowBlack;
                else if (rand < 98) objSpawn = heart;
                else if (rand < 100) objSpawn = star;
            }
            else if (count < 77)
            {
                Debug.Log("wave6");
                if (rand < 10) objSpawn = fruit1;
                else if (rand < 20) objSpawn = fruit2;
                else if (rand < 30) objSpawn = fruit3;
                else if (rand < 40) objSpawn = fruit4;
                else if (rand < 43) objSpawn = redBubble;
                else if (rand < 46) objSpawn = greenBubble;
                else if (rand < 49) objSpawn = purpleBubble;
                else if (rand < 52) objSpawn = yellowBubble;
                else if (rand < 62) objSpawn = redBlack;
                else if (rand < 72) objSpawn = greenBlack;
                else if (rand < 84) objSpawn = purpleBlack;
                else if (rand < 95) objSpawn = yellowBlack;
                else if (rand < 98) objSpawn = heart;
                else if (rand < 100) objSpawn = star;
            }
            else if (count < 79)
            {
                objSpawn = null;
            }
            else if (count < 100)
            {
                if (rand < 15) objSpawn = fruit1;
                else if (rand < 30) objSpawn = fruit2;
                else if (rand < 45) objSpawn = fruit3;
                else if (rand < 60) objSpawn = fruit4;
                else if (rand < 61) objSpawn = redBubble;
                else if (rand < 62) objSpawn = greenBubble;
                else if (rand < 63) objSpawn = purpleBubble;
                else if (rand < 64) objSpawn = yellowBubble;
                else if (rand < 65) objSpawn = bigRed;
                else if (rand < 66) objSpawn = bigGreen;
                else if (rand < 67) objSpawn = bigPurple;
                else if (rand < 68) objSpawn = bigYellow;
                else if (rand < 74) objSpawn = redBlack;
                else if (rand < 80) objSpawn = greenBlack;
                else if (rand < 86) objSpawn = purpleBlack;
                else if (rand < 94) objSpawn = yellowBlack;
                else if (rand < 98) objSpawn = heart;
                else if (rand < 100) objSpawn = star;
            }
            else if (count < 102)
            {
                objSpawn = null;
            }
            else if (count < 107)
            {
                if (rand < 15) objSpawn = fruit1;
                else if (rand < 30) objSpawn = fruit2;
                else if (rand < 45) objSpawn = fruit3;
                else if (rand < 60) objSpawn = fruit4;
                else if (rand < 62) objSpawn = bigRed;
                else if (rand < 64) objSpawn = bigGreen;
                else if (rand < 66) objSpawn = bigPurple;
                else if (rand < 68) objSpawn = bigYellow;
                else if (rand < 76) objSpawn = redBlack;
                else if (rand < 84) objSpawn = greenBlack;
                else if (rand < 92) objSpawn = purpleBlack;
                else if (rand < 100) objSpawn = yellowBlack;
            }
            else if (count < 115)
            {
                objSpawn = fruit1;
            }
            else if (count < 123)
            {
                if (rand < 14) objSpawn = fruit1;
                else if (rand < 28) objSpawn = fruit2;
                else if (rand < 42) objSpawn = fruit3;
                else if (rand < 56) objSpawn = fruit4;
                else if (rand < 58) objSpawn = redBubble;
                else if (rand < 60) objSpawn = greenBubble;
                else if (rand < 62) objSpawn = purpleBubble;
                else if (rand < 64) objSpawn = yellowBubble;
                else if (rand < 65) objSpawn = bigRed;
                else if (rand < 66) objSpawn = bigGreen;
                else if (rand < 67) objSpawn = bigPurple;
                else if (rand < 68) objSpawn = bigYellow;
                else if (rand < 74) objSpawn = redBlack;
                else if (rand < 80) objSpawn = greenBlack;
                else if (rand < 86) objSpawn = purpleBlack;
                else if (rand < 95) objSpawn = yellowBlack;
                else if (rand < 98) objSpawn = heart;
                else if (rand < 100) objSpawn = star;
            }
            else if (count < 126)
            {
                objSpawn = null;
            }
            else if (count < 142)
            {
                if (rand < 22) objSpawn = bigRed;
                else if (rand < 45) objSpawn = bigGreen;
                else if (rand < 68) objSpawn = bigPurple;
                else if (rand < 95) objSpawn = bigYellow;
                else if (rand < 98) objSpawn = heart;
                else if (rand < 100) objSpawn = star;
            }
            else if (count < 144)
            {
                objSpawn = null;
            }
            else if (count < 170)
            {
                if (rand < 12) objSpawn = fruit1;
                else if (rand < 24) objSpawn = fruit2;
                else if (rand < 36) objSpawn = fruit3;
                else if (rand < 48) objSpawn = fruit4;
                else if (rand < 51) objSpawn = redBubble;
                else if (rand < 54) objSpawn = greenBubble;
                else if (rand < 57) objSpawn = purpleBubble;
                else if (rand < 60) objSpawn = yellowBubble;
                else if (rand < 62) objSpawn = bigRed;
                else if (rand < 64) objSpawn = bigGreen;
                else if (rand < 66) objSpawn = bigPurple;
                else if (rand < 68) objSpawn = bigYellow;
                else if (rand < 74) objSpawn = redBlack;
                else if (rand < 80) objSpawn = greenBlack;
                else if (rand < 86) objSpawn = purpleBlack;
                else if (rand < 95) objSpawn = yellowBlack;
                else if (rand < 98) objSpawn = heart;
                else if (rand < 100) objSpawn = star;
            }
            else objSpawn = null;

            //==========================================================
            if (isFirst == true)
            {
                isFirst = false;
                if (count == 51)
                {
                    Instantiate(cloud1, new Vector3(15, Random.Range(-3.5f, 3.5f), 0), Quaternion.identity);
                    Instantiate(cloud2, new Vector3(26, Random.Range(-3.5f, 3.5f), 0), Quaternion.identity);
                }
                if (count == 28) objSpawn = boss1;
                if (count == 80) objSpawn = boss2;
                if (count == 17) timeSpawn -= 0.125f;

                if (count == 52) timeSpawn = 0.3f;
                if (count == 53) timeSpawn = 3;
                if (count == 56) timeSpawn = 0.25f;
                if (count == 57) timeSpawn = 5;
                if (count == 62) timeSpawn = oldTimeSpawn;

                if (count == 80) timeSpawn -= 0.15f;

                if (count == 103) timeSpawn = 0.3f;
                if (count == 104) timeSpawn = 3;
                if (count == 107) timeSpawn = 0.25f;
                if (count == 108) timeSpawn = 3;
                if (count == 110) timeSpawn = 0.175f;
                if (count == 111) timeSpawn = 5f;
                if (count == 116) timeSpawn = oldTimeSpawn - 0.13f;
                if (count == 126) timeSpawn += 0.05f;
                if (count == 141) timeSpawn -= 0.1f;
            }
        }
        else //TIME MODE
        {
            if (rand < 15) objSpawn = fruit1;
            else if (rand < 30) objSpawn = fruit2;
            else if (rand < 45) objSpawn = fruit3;
            else if (rand < 60) objSpawn = fruit4;
            else if (rand < 63) objSpawn = redBubble;
            else if (rand < 66) objSpawn = greenBubble;
            else if (rand < 69) objSpawn = purpleBubble;
            else if (rand < 72) objSpawn = yellowBubble;
            else if (rand < 75) objSpawn = bigRed;
            else if (rand < 78) objSpawn = bigGreen;
            else if (rand < 81) objSpawn = bigPurple;
            else if (rand < 83) objSpawn = bigYellow;
            else if (rand < 86) objSpawn = redBlack;
            else if (rand < 89) objSpawn = greenBlack;
            else if (rand < 92) objSpawn = purpleBlack;
            else if (rand < 95) objSpawn = yellowBlack;
            else if (rand < 98) objSpawn = star;
            else if (rand < 100) Instantiate(cloud1, new Vector3(15, Random.Range(-3.5f, 3.5f), 0), Quaternion.identity);

            if (count == 20) objSpawn = boss1;
            if (count == 40) objSpawn = boss2;
        }

        float x, y;
        x = Random.Range(-3, 3);
        y = 5f;

        if (objSpawn != null)
        {
            if (objSpawn.tag == "Boss")
            {
                y = 7;
                Instantiate(objSpawn, new Vector3(0, 0, 0), Quaternion.identity);
            }
            else if (objSpawn.tag == "Bubble")
            {
                y = 7;
                Instantiate(objSpawn, new Vector3(x, y, 0), Quaternion.identity);
            }
            else if (objSpawn.tag == "Item")
            {
                y = 7;
                Instantiate(objSpawn, new Vector3(x, y, 0), Quaternion.identity);
            }
            else
            {
                float randAngle = Random.Range(0, 2 * Mathf.PI);
                Instantiate(objSpawn, new Vector3(x, y, 0), Quaternion.EulerAngles(0, 0, randAngle));
            }
        }
        else Debug.Log("nullobj");

        //count++;
        Invoke("SpawnObj", timeSpawn);
        // Debug.Log(count + " " + rand);
    }

    void Count()
    {
        isFirst = true;
        count++;
        if (count == 148)
        {
            count = 0;
            oldTimeSpawn -= 0.3f;
            if (oldTimeSpawn < 0.5f) oldTimeSpawn = 0.5f;
            timeSpawn = oldTimeSpawn;
            GameObject bg = GameObject.FindGameObjectWithTag("Background");
            bg.GetComponent<Animator>().Play("BackgroundDay");
        }
        Invoke("Count", 1);
    }
}
