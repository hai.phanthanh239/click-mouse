using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigFruit : MonoBehaviour
{
    GameObject obj;
    Rigidbody2D rigi;
    Animator anim;

    Vector3 mousePos;

    [SerializeField]
    private GameObject plusPoint, fruit, pop;

    public float speedFruitx, speedFruity;
    public bool isTimeMode;

    private void Awake()
    {
        obj = gameObject;
        rigi = obj.GetComponent<Rigidbody2D>();
        anim = obj.GetComponent<Animator>();

        rigi.gravityScale = 0;
        Invoke("Fall", 1.3f);
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (isTimeMode)
        {
            if (GameCtrlTimeMode.isClickStar)
            {
                GameCtrlTimeMode.point += 3;
                Instantiate(plusPoint, transform.position, Quaternion.identity);
                Destroy(obj);
            }
        }
        else
        {
            if (GameController.isClickStar)
            {
                GameController.point += 3;
                Instantiate(plusPoint, transform.position, Quaternion.identity);
                Destroy(obj);
            }
        }
    }

    void Fall()
    {
        rigi.gravityScale = 0.3f;
    }

    private void OnMouseDown()
    {
        //luc nhan vao se phan than thanh 2 fruit nho hon
        GameObject fruitObj = fruit;
        fruitObj.GetComponent<Fruit>().isInBubble = true;

        fruitObj = Instantiate(fruit, transform.position - new Vector3(0.5f,0,0), Quaternion.identity);
        fruitObj.GetComponent<Rigidbody2D>().velocity = new Vector3(-speedFruitx*Time.fixedDeltaTime, speedFruity*Time.fixedDeltaTime, 0);      

        fruitObj = Instantiate(fruit, transform.position + new Vector3(0.5f,0,0), Quaternion.identity);
        fruitObj.GetComponent<Rigidbody2D>().velocity = new Vector3(speedFruitx * Time.fixedDeltaTime, speedFruity*Time.fixedDeltaTime, 0);

        fruit.GetComponent<Fruit>().isInBubble = false;

        Destroy(obj);
    }

    private void OnDestroy()
    {
        Instantiate(pop, transform.position, Quaternion.identity);
    }
}
