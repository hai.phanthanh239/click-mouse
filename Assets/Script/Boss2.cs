using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2 : MonoBehaviour
{
    Animator anim;
    public GameObject pop, plus10;
    GameObject box,cameraObj;

    public AudioClip shotClip, roarClip;
    AudioSource music;
    public int HP;

    bool isCanBeHit;
    public bool isTimeMode;

    private void Awake()
    {
        anim = gameObject.GetComponent<Animator>();
        music = gameObject.GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        cameraObj = GameObject.FindGameObjectWithTag("MainCamera");
        box = GameObject.FindGameObjectWithTag("BoxInclude");
        box.GetComponent<Animator>().SetBool("isReturn", false);
        isCanBeHit = false;
        Invoke("Attack", 1.5f);
        Invoke("RunAway", 10);
    }

    private void FixedUpdate()
    {

    }

    void Attack()
    {
        music.clip = roarClip;
        music.Play();
        cameraObj.GetComponent<CameraController>().Zoom();
        box.GetComponent<Animator>().Play("BoxStartMove");
        Invoke("CanBeHit", 1f);
    }

    void CanBeHit()
    {
        isCanBeHit = true;
    }

    private void OnMouseDown()
    {
        if (isCanBeHit)
        {
            music.clip = shotClip;
            music.Play();
            anim.Play("get_hit");
            HP--;
            if (HP <= 0)
            {
                Instantiate(pop, transform.position, Quaternion.identity);
                Instantiate(plus10, transform.position, Quaternion.identity);
                if (isTimeMode) GameCtrlTimeMode.point += 10;
                else GameController.point += 10;
                Destroy(gameObject);
            }
        }
    }

    void RunAway()
    {
        isCanBeHit = false;
        anim.Play("run_away");
        Destroy(gameObject, 2);
    }

    private void OnDestroy()
    {
        box.GetComponent<Animator>().SetBool("isReturn", true);
    }

}
